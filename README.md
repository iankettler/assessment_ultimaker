# Ultimaker Assessment

I have chosen to use the built in development version of php as a webserver in order to run a local copy of the website and to run all tests with a selenium standalone server in combination with nightwatch

You will need the Java SE Development Kit in order to make Selenium tests possible, you can see for yourself if you have installed the runtime by executing the following console command ... 

```sh
$ java -version
```

... and look for the following string: "Java\(TM\) SE Runtime Environment". If not available on your system you can download and install a fresh copy at: [JDK8 Download](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

To install the website you can git clone the repo with the following command:

```sh
$ git clone https://bitbucket.org/iankettler/assessment_ultimaker.git ultimaker_ian
$ cd ultimaker_ian
```

I have programmed all CSS with the libsass pre-processor in order to have the fastest gcc compiling options available. You don't need to install libsass as a compiled version of the CSS is already available in the source tree.

The following open source projects have been used in this project/stack:

  - NPM
  - PhP
  - Twig
  - libsass
  - Nightwatch / PhantomJS
  - Bootstrap version 4 - alpha v2 (loaded via CDN)
  - Composer
  - Git
  - Bower
  - Javascript / jQuery (loaded via CDN)
  - FireFox

I have used CodeKit for front-end development and compiling/minifying the .js files and to monitor file changes for reloading in the browser. I did compile the minified versions of the .js files but didn't include them in the final html as it is more readable for the developers to see the code inside the browser console. I have used two fonts, one of them is being used to have the tailormade icons in the interface. separated from the default glyph-icons in Bootstrap and thus the vector font (fontello) does have a relative small footprint.

# Installation

After downloading the repo, the following terminal commands need to be invoked:

### check if NPM is installed
```sh
$:ultimaker_ian $ npm -v
> 3.6.0
```
### install dependencies
```sh
$:ultimaker_ian $ npm update && npm install
```

### install selenium standalone server
```sh
$:ultimaker_ian $ sudo npm install selenium-standalone@latest -g
$:ultimaker_ian $ sudo selenium-standalone install
```

### add directory 'upload' in the root
```sh
$:ultimaker_ian $ mkdir upload
$:ultimaker_ian $ chmod 755 upload
```

# Running the website

I recommend using the selenium test suite in order to run all tests. For this you will have to install the FireFox browser or configure the chromedriver for selenium yourself. After the last test is finished the browser will stay open and you're free to browse the website by yourself.

To run the tests:

### Start PHPs built in development server on port 4444

First terminal tab:
```sh
$:ultimaker_ian $ php -S localhost:4444
```

### Start the Selenium standalone server

Second terminal tab:
```sh
$:ultimaker_ian $ selenium-standalone start
```

### Start the Selenium tests from Nightwatch

Third terminal tab:
```sh
$:ultimaker_ian $ cd node_modules/nightwatch
$:ultimaker_ian $ ./bin/nightwatch -t ../../_tests/test-ultimaker-upload-website.js
# After the test "Running:  Overwrite uploaded file" has passed, you can browse the website by yourself.
```