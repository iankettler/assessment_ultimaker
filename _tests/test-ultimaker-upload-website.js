module.exports = (function(settings) {
  settings.test_workers = false;
  return settings;
})(require('./nightwatch.json'));

// to disable individual test add ''+ in front of the function, example:
// 'Assessment Ultimaker website running': ''+function (browser) {
module.exports = {
  'Assessment Ultimaker website running': function(browser) {
    browser
      .url('http://localhost:4444/') // make sure the website is up and running
      .waitForElementVisible('body', 1000)
      .assert.title('Ultimaker: Upload your 3D model')
      .end();
  },

  'Upload file': function(browser) {
    var path = require('path');
    var testFileCube = path.resolve(path.join(__dirname, 'testImages/rubiks-cube.svg'));

    browser
      .url('http://localhost:4444/') // make sure the website is up and running
      .pause(2000);

      browser.waitForElementVisible('body', 1000);
      browser.assert.title('Ultimaker: Upload your 3D model');
      browser.expect.element('input#file').to.be.present;
      browser.expect.element('input#file').to.not.be.visible;
      browser.expect.element('#file').to.be.an('input')
      browser.assert.containsText("label#default", "Select");
      browser.setValue('input#file', testFileCube);
      browser.getValue("form input#file[type=file]", function(result) {
        this.assert.equal(typeof result, "object");
      });
      browser.waitForElementVisible('#upload', 1000);
      browser.click('#upload');
      browser.end();
  },

  'Upload wrong type file': function(browser) {
    var path = require('path');
    var testFileWrongType = path.resolve(path.join(__dirname, 'testImages/wrongTypeOfFile.txt'));

    browser
      .url('http://localhost:4444/') // make sure the website is up and running
      .pause(2000);

      browser.waitForElementVisible('body', 1000);
      browser.assert.title('Ultimaker: Upload your 3D model');
      browser.expect.element('input#file').to.be.present;
      browser.expect.element('input#file').to.not.be.visible;
      browser.expect.element('#file').to.be.an('input')
      browser.assert.containsText("label#default", "Select");
      browser.setValue('input#file', testFileWrongType);
      browser.getValue("form input#file[type=file]", function(result) {
        this.assert.equal(typeof result, "object");
      });
      browser.pause(1000);
      browser.expect.element('.alert').to.be.visible;
      browser.assert.containsText(".alert strong", "Wrong image type");
      browser.end();
  },

  'Upload file and request more information': function(browser) {
    var path = require('path');
    var testFileCube = path.resolve(path.join(__dirname, 'testImages/rubiks-cube.svg'));

    browser
      .url('http://localhost:4444/') // make sure the website is up and running
      .pause(2000);

      browser.waitForElementVisible('body', 1000);
      browser.assert.title('Ultimaker: Upload your 3D model');
      browser.expect.element('input#file').to.be.present;
      browser.expect.element('input#file').to.not.be.visible;
      browser.expect.element('#file').to.be.an('input')
      browser.assert.containsText("label#default", "Select");
      browser.setValue('input#file', testFileCube);
      browser.getValue("form input#file[type=file]", function(result) {
        this.assert.equal(typeof result, "object");
      });
      browser.waitForElementVisible('#upload', 1000);
      browser.click('#upload');
      browser.pause(1000);
      browser.click('.icon-info-circled-alt');
      browser.end();
  },

  'Remove uploaded file': function(browser) {
    var path = require('path');
    var testFileCube = path.resolve(path.join(__dirname, 'testImages/rubiks-cube.svg'));

    browser
      .url('http://localhost:4444/') // make sure the website is up and running
      .pause(2000);

      browser.waitForElementVisible('body', 1000);
      browser.assert.title('Ultimaker: Upload your 3D model');
      browser.expect.element('input#file').to.be.present;
      browser.expect.element('input#file').to.not.be.visible;
      browser.expect.element('#file').to.be.an('input')
      browser.assert.containsText("label#default", "Select");
      browser.setValue('input#file', testFileCube);
      browser.getValue("form input#file[type=file]", function(result) {
        this.assert.equal(typeof result, "object");
      });
      browser.waitForElementVisible('#upload', 1000);
      browser.click('#upload');
      browser.pause(1000);
      browser.click('.dropdown-toggle');
      browser.pause(500);
      browser.click('.dropdown-item');
      browser.pause(2000);
      browser.click('#removeFilePermanent');
      browser.end();
  },

  'Overwrite uploaded file': function(browser) {
    var path = require('path');
    var testFileCube = path.resolve(path.join(__dirname, 'testImages/rubiks-cube.svg'));
    var testFileLogo = path.resolve(path.join(__dirname, 'testImages/ultimaker-logo.svg'));

    browser
      .url('http://localhost:4444/') // make sure the website is up and running
      .pause(2000);

      browser.waitForElementVisible('body', 1000);
      browser.assert.title('Ultimaker: Upload your 3D model');
      browser.expect.element('input#file').to.be.present;
      browser.expect.element('input#file').to.not.be.visible;
      browser.expect.element('#file').to.be.an('input')
      browser.assert.containsText("label#default", "Select");
      browser.setValue('input#file', testFileCube);
      browser.getValue("form input#file[type=file]", function(result) {
        this.assert.equal(typeof result, "object");
      });
      browser.waitForElementVisible('#upload', 1000);
      browser.click('#upload');
      browser.pause(2000);
      browser.setValue('input#file', testFileLogo);
      browser.getValue("form input#file[type=file]", function(result) {
        this.assert.equal(typeof result, "object");
      });
      browser.waitForElementVisible('#upload', 1000);
      browser.click('#upload');
      browser.pause();
  }
};