<?php

// error handling
ini_set('display_errors', 1); 
error_reporting(E_ALL);

$ds = DIRECTORY_SEPARATOR;

function curPageURL()
{
    $pageURL = 'http';
    if (isset($_SERVER["HTTPS"]) && strtolower($_SERVER["HTTPS"]) == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

$murl = curPageURL();
parse_str($murl, $result);

$storeFolder = 'upload' . $ds;

if (!is_dir($storeFolder)) {
    mkdir($storeFolder, 0777);
}

if (!empty($_FILES)) {
    $tempFile   = $_FILES['file']['tmp_name'][0];
    $targetPath = dirname(__FILE__) . $ds . $storeFolder;
    $targetFile = $targetPath . $_FILES['file']['name'][0];


	if (file_exists($targetFile)) {
	    unlink($targetFile);
	}

    move_uploaded_file($tempFile, $targetFile);
    echo json_encode( array('targetFile' => $targetFile) );
}

/*
echo json_encode( array(
	'murl' => $murl,
	'storeFolder' => $storeFolder,
	'tempFile' => $tempFile,
	'targetPath' => $targetPath,
	'targetFile' => $targetFile,
	'status' => 'ok')
);
*/
?>