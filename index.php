<?php

/*----------------------------------------------------------
  Include Twig autoloader
  ----------------------------------------------------------*/
require 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

/*----------------------------------------------------------
  Set block tags for better readablitty of Twig elements/blocks
  and avoid inline script to be used
  ----------------------------------------------------------*/
$lexer = new Twig_Lexer($twig, array(
	'tag_block' => array('{%','%}'),
	'tag_variable' => array('{{ $','}}')
));

$twig->setLexer($lexer);

/*----------------------------------------------------------
  Index template variables
  ----------------------------------------------------------*/
echo $twig->render('index.html', array(
    'title' => 'Upload your 3D model',
    'hidden_image_information' => 'Some information about your uploaded image like specifications',
    'defaultImageText' => 'After uploading, your model will show here',
    'instruction_text' => 'This form allows you to upload a file to the server.',
    'form_text_prefix' => 'File input',
    'form_text_input' => 'No file selected for upload',
    'form_text_button_default' => 'Select',
    'form_text_button_upload' => 'Upload'
));

?>