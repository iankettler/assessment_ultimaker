;(function ( $, document, undefined ) {
	// show the right button of the form at any position
	function showButton(button) {
		if (button === 'upload') {
			defaultField.hide();
			uploadField.show();
		} else if (button === 'default') {
			defaultField.show();
			uploadField.hide();
		}
	}

	// show the right button of the form at any position
	function showPreviewImg(area) {
		if (area === 'preUpload') {
			postImgPreview.hide();
			preImgPreview.show();
		} else if (area === 'postUpload') {
			postImgPreview.show();
			preImgPreview.hide();
		}
	}
	// handle progress percentage for progress bar
	function progressHandlerFunction(e) {
		if (e.lengthComputable) {
			var percentComplete = Math.round((e.loaded / e.total) * 100);

			$('progress').attr('value', percentComplete);
			$('.progress-bar').css('width', percentComplete + '%');
		}
	}

	var progressField		= $('.progress'),
		defaultField		= $('#default'),
		uploadField			= $('#upload'),
		filetext 			= $('#filename'),
		filetextOrig		= $('#filename').attr("placeholdertext"),
		preImgPreview		= $('.img-uploader__pre_upload'),
		postImgPreview		= $('.img-uploader__post_upload');
		
	
	$(window).load(function(e) {
		e.preventDefault();
		console.log('#-> LOAD');
		console.log(e);

		// disbale dropdown form element
		$('.dropdown-toggle').prop('disabled', true);
		// hide more information button
		$('.icon-info-circled-alt').addClass('hidden');
		// show select button in form
		showButton('default');
		// show pre upload area
		showPreviewImg('preUpload');
	});

	$('form#fileUploader').change(function(e) {
		e.preventDefault();
		console.log('#-> CHANGE');
		console.log(e);

		var uploadedImg = $('input[type=file]')[0].files[0];

		var fr = new FileReader();
		fr.onload = function() {
			// file is loaded
			var img = new Image();

			$('.alert-danger-wrong-file-type').removeClass('hidden');

			img.onload = function() {
				// get current fyle type
				var fileType = uploadedImg.type;

				// validation of valid file types : image/jpeg | image/svg+xml | image/png | image/gif
				var allowedFileFormats = ['image/jpeg','image/svg+xml','image/png','image/gif'];

				$(allowedFileFormats).each( function(i, value) {

					if ( value === fileType ) {
						// show pre upload area
						showPreviewImg('preUpload');

						// show filename in input field
						$('#filename').attr('placeholder', uploadedImg.name);

						// show loader
						progressField.toggle('progress-hidden');

						// show upload button in form
						showButton('upload');

						$('.alert-danger-wrong-file-type').addClass('hidden');
						
						return false;

					}
				});
			};

		    // is the data URL because called with readAsDataURL
		    img.src = fr.result;
		};

		fr.readAsDataURL( uploadedImg );
		
	});

	$('#upload').on('click', function(e) {
		e.preventDefault();
		console.log('#-> SUBMIT');
		console.log(e);

		var form = $('form#fileUploader');
		var formdata = false;
		if (window.FormData){
			formdata = new FormData(form[0]);
		}

		$.ajax({
			url				: '/upload.php',
			data			: formdata ? formdata : form.serialize(),
			cache			: false,
			contentType		: false,
			processData		: false,
			type			: 'POST',
            dataType		: 'json',
            xhr: function() {
				var xhr = $.ajaxSettings.xhr();
				xhr.upload.addEventListener('progress', progressHandlerFunction, false);
				return xhr;
			},
			success: function(data){
			    // Callback

			    // close loader
				progressField.toggle('progress-hidden');

			    // get image part of JSON string
			    var imgRes = '/upload/' + data.targetFile.split('/').pop();

				// this should not be undefined
				$('.img-uploader__post_upload img')
					.attr('src', imgRes)
					.css('max-height', '100%')
					.css('min-height', 'auto')
					.css('max-width', '100%')
					.css('min-width', 'auto')
					.css('width', 'auto')
  					.css('height', 'auto');

  				// show more information button
				$('.icon-info-circled-alt').removeClass('hidden');

				// disable dropdown form element
				$('.dropdown-toggle').prop('disabled', false);

				// remove file from form
				$("#file").replaceWith($("#file").clone());

				// remove filename from input
				filetext.attr( 'placeholder', filetextOrig );

				// show img in post upload area
				showPreviewImg('postUpload');

				// reset progress bar
				$('progress').attr('value', 0);
				$('.progress-bar').css('width', '0%');

			    showButton('default');

			},
			error: function() {
				console.log("error in AJAX upload");
			}
		});
		return false;
	});

	$('#removeFilePermanent').click(function() {
		// show img in post upload area
		showPreviewImg('preUpload');
		// hide more information button
		$('.icon-info-circled-alt').addClass('hidden');
		// disable dropdown form element
		$('.dropdown-toggle').prop('disabled', true);
		// hide modal on delete action
		$('#fileDelete').modal('hide');
	});

}(jQuery, document));
