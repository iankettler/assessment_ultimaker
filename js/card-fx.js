$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });

    }
});

;(function() {

    // function to animate objects and remove or add classes
    // according to the requested action
    var animationAction = function( element, animationName, action ) {

        element.animateCss( animationName );

        if ( action === 'removeClass' ) {
            element.removeClass( 'visible' );
        } else if ( action === 'addClass' ) {
            element.addClass( 'visible' );
        }
        
    };

    // collect text to animate
    var $arr_animText = $('.card-title__wrapper > b');

    // capture click on information button
    $('.jQuery-header-swap').click( function() {

        $.each( $arr_animText , function() {

            if ( $(this).hasClass( 'visible' )) {

                if( $(this).hasClass( 'first')) {
                    animationAction( $(this), 'slideInUp', 'removeClass' );
                } else if( $(this).hasClass( 'second')) {
                    animationAction( $(this), 'slideInDown', 'removeClass' );
                }
                
            } else {

                if( $(this).hasClass( 'first')) {
                    animationAction( $(this), 'slideInUp', 'addClass' );
                } else if( $(this).hasClass( 'second')) {
                    animationAction( $(this), 'slideInDown', 'addClass' );
                }
                
            }

        });

    });

})(jQuery);